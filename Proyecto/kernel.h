#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "Measure.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <opencv2/opencv.hpp>

typedef unsigned char ubyte;
typedef unsigned int uint;
typedef unsigned short ushort;

cudaError_t cuda_setupf(float* m_data, float* m_resultData,
	size_t m_worldWidth, size_t m_worldHeight, size_t m_dataLength,
	size_t iterationsCount, ushort threadsCount);

cudaError_t cuda_setup_simple(std::ifstream& filenames,
	size_t m_imageWidth, size_t m_imageHeight, size_t m_dataLength,
	size_t n_bins, ushort threadsCount,
	Measure measure);

cudaError_t cuda_setup_pond(std::ifstream& fs,
	size_t m_imageWidth, size_t m_imageHeight, size_t m_dataLength,
	size_t n_bins, ushort threadsCount,
	Measure measure);

cudaError_t cuda_setup_pond_int_lin(std::ifstream& fs,
	size_t m_imageWidth, size_t m_imageHeight, size_t m_dataLength,
	size_t n_bins, ushort threadsCount,
	Measure measure);