from pathlib import Path

folderpath = Path("./files/")
files = folderpath.glob("*/*.*")

with open("filenames.txt", "w") as f:
	for path in files:
		f.write(str(path))
		f.write("\n")