#include "Measure.h"
#include <fstream>
#include <iostream>
#include <utility>
#include <vector>
#include <chrono>


using namespace std;

bool file_exists(std::string s) {
	fstream fs(s);
	return fs.good();
}

Measure::Measure(int threads, int width, int height, int n_bins, int n_images) : 
	threadsCount{ threads },
	m_Width {width}, m_Height {height},
	n_bins {n_bins}, n_images {n_images}
{}

void Measure::start_exp() {
	start = chrono::high_resolution_clock::now();
}
void Measure::finish_exp() {
	finish = chrono::high_resolution_clock::now();
}
double Measure::get_run_time() {
	auto duration = chrono::duration_cast<chrono::microseconds>(finish - start).count();
	return static_cast<double>(duration) / 1000000.0;
}
int Measure::get_Width()
{
	return m_Width;
}
int Measure::get_Height()
{
	return m_Height;
}
int Measure::get_threads_count()
{
	return threadsCount;
}
int Measure::get_n_bins()
{
	return n_bins;
}
int Measure::get_n_images()
{
	return n_images;
}

/*
Writes results to results.txt opening the file with append
Input:
	int	mode: 0 is simple, 1 is weighed, 2 is interpolated
*/
void Measure::write_info(int mode) 
{
	string results_file = "results.txt";
	ofstream fs;
	if (file_exists(results_file)) {
		fs.open(results_file, fstream::out | fstream::in | fstream::app);
	}
	else {
		fs.open(results_file, fstream::out | fstream::app);
		fs << "mode,width,height,datalength,n_bins,n_images,threadcount_blocksize,runtime" << endl;
		fs.flush();
	}
	string runtime = std::to_string(get_run_time());

	fs << mode;
	fs << ",";

	fs << get_Width();
	fs << ",";

	fs << get_Height();
	fs << ",";

	fs << m_Width*m_Height;
	fs << ",";

	fs << get_n_bins();
	fs << ",";

	fs << get_n_images();
	fs << ",";

	fs << get_threads_count();
	fs << ",";

	fs << runtime;
	fs << endl;

	fs.close();
}

/*
Writes feature vector to fv.txt file opening the file with append
Input:
	float*&	fv: feature vector
*/
void Measure::write_fv(float*& fv)
{
	string results_file = "fv.txt";
	ofstream fs;
	if (file_exists(results_file)) {
		fs.open(results_file, fstream::out | fstream::in | fstream::app);
	}
	else {
		fs.open(results_file, fstream::out | fstream::app);
	}
	for (int i = 0; i < n_bins; i++) {
		fs << fv[i];
		if (i != n_bins - 1)
			fs << ",";
	}
	fs << endl;
	fs.close();
}

/*
Writes feature vector to fv.txt file opening the file with append
Input:
	int*&	fv: feature vector
*/
void Measure::write_fv(uint *& fv)
{
	string results_file = "fv.txt";
	ofstream fs;
	if (file_exists(results_file)) {
		fs.open(results_file, fstream::out | fstream::in | fstream::app);
	}
	else {
		fs.open(results_file, fstream::out | fstream::app);
	}
	for (int i = 0; i < n_bins; i++) {
		fs << fv[i];
		if (i != n_bins - 1)
			fs << ",";
	}
	fs << endl;
	fs.close();
}


