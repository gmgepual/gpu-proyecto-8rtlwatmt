#include "kernel.h"

#include <stdio.h>
#include <assert.h>
#include <algorithm>
#include <math.h>
#include <math_constants.h>


/*
Kernel for vertical sobel only
Input:
	float*	bw_image: array with image pixels
	uint	im_width: image width
	uint	im_height: image height
	float*	sobelx: array where results are saved
*/
__global__ void sobel_v_cyclic(const float* bw_image, uint im_width,
	uint im_height, float* sobelx) {
	uint data_length = im_width * im_height;

	for (uint idx = blockIdx.x * blockDim.x + threadIdx.x;
		idx < data_length;
		idx += blockDim.x * gridDim.x) 
	{
		uint x = idx % im_width;
		uint y = idx - x;
		uint xl1 = (x + data_length - 1) % im_width;
		uint xr1 = (x + 1) % im_width;
		uint yu1 = (y + data_length - im_width) % data_length;
		uint yd1 = (y + im_width) % data_length;

		sobelx[x + y] = -bw_image[xl1 + yu1] + bw_image[xr1 + yu1]
			+ -2 * bw_image[xl1 + y] + 2 * bw_image[xr1 + y]
			+ -bw_image[xl1 + yd1] + bw_image[xr1 + yd1];
	}
}

/*
Kernel for horizontal sobel only
Input:
	float*	bw_image: array with image pixels
	uint	im_width: image width
	uint	im_height: image height
	float*	sobely: array where results are saved
*/
__global__ void sobel_h_cyclic(const float* bw_image, uint im_width,
	uint im_height, float* sobely) {
	uint data_length = im_width * im_height;

	for (uint idx = blockIdx.x * blockDim.x + threadIdx.x;
		idx < data_length;
		idx += blockDim.x * gridDim.x) 
	{
		uint x = idx % im_width;
		uint y = idx - x;
		uint xl1 = (x + data_length - 1) % im_width;
		uint xr1 = (x + 1) % im_width;
		uint yu1 = (y + data_length - im_width) % data_length;
		uint yd1 = (y + im_width) % data_length;

		sobely[x + y] = bw_image[xl1 + yu1] + 2 * bw_image[x + yu1] + bw_image[xr1 + yu1]
			+ -bw_image[xl1 + yd1] + -2 * bw_image[x + yd1] + -bw_image[xr1 + yd1];
	}
}

/*
Kernel used to calculate magnitude of gradient vector
Input:
	float*	bw_image: array with image pixels
	uint	im_width: image width
	uint	im_height: image height
	float*	magnitude: array where results are saved
*/
__global__ void gradient_m(const float* bw_image, uint im_width,
	uint im_height, float* magnitude) {
	uint data_length = im_width * im_height;

	for (uint idx = blockIdx.x * blockDim.x + threadIdx.x;
		idx < data_length;
		idx += blockDim.x * gridDim.x) 
	{
		uint x = idx % im_width;
		uint y = idx - x;
		uint xl1 = (x + data_length - 1) % im_width;
		uint xr1 = (x + 1) % im_width;
		uint yu1 = (y + data_length - im_width) % data_length;
		uint yd1 = (y + im_width) % data_length;

		float sobelx = -bw_image[xl1 + yu1] + bw_image[xr1 + yu1]
			+ -2 * bw_image[xl1 + y] + 2 * bw_image[xr1 + y]
			+ -bw_image[xl1 + yd1] + bw_image[xr1 + yd1];
		float sobely = bw_image[xl1 + yu1] + 2 * bw_image[x + yu1] + bw_image[xr1 + yu1]
			+ -bw_image[xl1 + yd1] + -2 * bw_image[x + yd1] + -bw_image[xr1 + yd1];

		magnitude[x + y] = sqrt(sobelx*sobelx + sobely * sobely);
	}
}

/*
Kernel used to calculate orientation of gradient vector
Input:
	float*	bw_image: array with image pixels
	uint	im_width: image width
	uint	im_height: image height
	float*	orientation: array where results are saved
*/
__global__ void gradient_o(const float* bw_image, uint im_width,
	uint im_height, float* orientation) {
	uint data_length = im_width * im_height;

	for (uint idx = blockIdx.x * blockDim.x + threadIdx.x;
		idx < data_length;
		idx += blockDim.x * gridDim.x) 
	{
		uint x = idx % im_width;
		uint y = idx - x;
		uint xl1 = (x + data_length - 1) % im_width;
		uint xr1 = (x + 1) % im_width;
		uint yu1 = (y + data_length - im_width) % data_length;
		uint yd1 = (y + im_width) % data_length;

		float sobelx = -bw_image[xl1 + yu1] + bw_image[xr1 + yu1]
			+ -2 * bw_image[xl1 + y] + 2 * bw_image[xr1 + y]
			+ -bw_image[xl1 + yd1] + bw_image[xr1 + yd1];
		float sobely = bw_image[xl1 + yu1] + 2 * bw_image[x + yu1] + bw_image[xr1 + yu1]
			+ -bw_image[xl1 + yd1] + -2 * bw_image[x + yd1] + -bw_image[xr1 + yd1];

		orientation[x + y] = atan2(sobely, sobelx);
	}
}

/*
Kernel used to get which pixel contributes to which bin (based on flooring function applied to distributing by orientation)
Input:
	float*	orientation: array with orientations of image
	uint	im_width: image width
	uint	im_height: image height
	uint	n_bins: number of bins
	uint*	h_matrix: array where results are saved
*/
__global__ void gradient_h_floor(const float* orientation, uint im_width,
	uint im_height, uint n_bins, uint* h_matrix) {
	uint data_length = im_width * im_height;

	for (uint idx = blockIdx.x * blockDim.x + threadIdx.x;
		idx < data_length;
		idx += blockDim.x * gridDim.x) 
	{
		uint x = idx % im_width;
		uint y = idx - x;

		float ro = roundf(orientation[x + y] / CUDART_PI_F * ((float)n_bins));

		h_matrix[x + y] = ((uint) floor(orientation[x + y] / CUDART_PI_F * ((float)n_bins)) % n_bins);
	}
}

/*
Kernel used to get which pixel contributes to which bin (based on ceiling function applied to distributing by orientation)
Input:
	float*	orientation: array with gradient orientations of image
	uint	im_width: image width
	uint	im_height: image height
	uint	n_bins: number of bins
	uint*	h_matrix: array where results are saved
*/
__global__ void gradient_h_ceil(const float* orientation, uint im_width,
	uint im_height, uint n_bins, uint* h_matrix) {
	uint data_length = im_width * im_height;

	for (uint idx = blockIdx.x * blockDim.x + threadIdx.x;
		idx < data_length;
		idx += blockDim.x * gridDim.x) 
	{
		uint x = idx % im_width;
		uint y = idx - x;

		float ro = roundf(orientation[x + y] / CUDART_PI_F * ((float)n_bins));

		h_matrix[x + y] = (((uint) ceil(orientation[x + y] / CUDART_PI_F * ((float)n_bins))) % n_bins);
	}
}

/*
Kernel to get simple histogram (add 1 to every contribution)
Input:
	uint*	h_matrix_floor: array with results of gradient_h_floor Kernel
	uint	im_width: image width
	uint	im_height: image height
	uint	n_bins: number of bins
	uint*	histogram: array where histogram results are saved
*/
__global__ void histogram_simple(const uint* h_matrix_floor, uint im_width,
	uint im_height, uint n_bins, uint* histogram) {
	uint data_length = im_width * im_height;

	for (uint idx = blockIdx.x * blockDim.x + threadIdx.x;
		idx < data_length;
		idx += blockDim.x * gridDim.x) 
	{
		uint x = idx % im_width;
		uint y = idx - x;

		atomicAdd(&(histogram[h_matrix_floor[x + y]]), 1);
	}
}

/*
Kernel to get simple histogram (add 1 to every contribution)
Input:
	float*	mag: array with gradient magnitudes of image
	uint*	h_matrix_floor: array with results of gradient_h_floor Kernel
	uint	im_width: image width
	uint	im_height: image height
	uint	n_bins: number of bins
	float*	histogram_f: array where histogram results are saved
*/
__global__ void histogram_mag(const float* mag, const uint* h_matrix_floor, uint im_width,
	uint im_height, uint n_bins, float* histogram_f) {
	uint data_length = im_width * im_height;

	for (uint idx = blockIdx.x * blockDim.x + threadIdx.x;
		idx < data_length;
		idx += blockDim.x * gridDim.x) 
	{
		uint x = idx % im_width;
		uint y = idx - x;

		atomicAdd(&(histogram_f[h_matrix_floor[x + y]]), mag[x + y]);
	}
}

/*
Kernel to get simple histogram (add 1 to every contribution)
Input:
	float*	mag: array with gradient magnitudes of image
	float*	orientation: array with gradient orientations of image
	uint	im_width: image width
	uint	im_height: image height
	uint	n_bins: number of bins
	float*	histogram_f: array where histogram results are saved
*/
__global__ void histogram_mag_lin_int(const float* mag, const float* orientation,
	uint im_width, uint im_height, uint n_bins, float* histogram_f) {
	uint data_length = im_width * im_height;

	for (uint idx = blockIdx.x * blockDim.x + threadIdx.x;
		idx < data_length;
		idx += blockDim.x * gridDim.x) 
	{
		uint x = idx % im_width;
		uint y = idx - x;
		
		float ro = orientation[x + y] / CUDART_PI_F * ((float)n_bins);
		uint ro_ceil = (((uint)ceil(orientation[x + y] / CUDART_PI_F * ((float)n_bins))));
		uint ro_floor = ((uint)floor(orientation[x + y] / CUDART_PI_F * ((float)n_bins)));

		float weight_ceil = ro_ceil - ro;
		float weight_floor = 1 - weight_ceil;

		atomicAdd(&(histogram_f[ro_ceil % n_bins]), weight_ceil*mag[x + y]);
		atomicAdd(&(histogram_f[ro_floor % n_bins]), weight_floor*mag[x + y]);
	}
}

/*
unused
*/
cudaError_t cuda_setupf(float* m_data, float* m_resultData,
	size_t m_worldWidth, size_t m_worldHeight, size_t m_dataLength,
	size_t n_bins, ushort threadsCount)
{
	float* dev_data = 0;
	float* dev_result = 0;

	cudaError_t cudaStatus;

	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&dev_data, m_dataLength * sizeof(float));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}


	cudaStatus = cudaMalloc((void**)&dev_result, m_dataLength * sizeof(float));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}


	// Copy input vectors from host memory to GPU buffers.
	cudaStatus = cudaMemcpy(dev_data, m_data, m_dataLength * sizeof(float), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}
	/*
	cudaEvent_t start, stop;
	float elapsedTime;
	cudaEventCreate(&start);
	cudaEventRecord(start, 0);
	*/

	size_t reqBlocksCount = (m_worldWidth * m_worldHeight) / threadsCount;
	ushort blocksCount = (ushort)std::min((size_t)32768, reqBlocksCount);

	sobel_h_cyclic << <blocksCount, threadsCount >> > (dev_data, m_worldWidth,
		m_worldHeight, dev_result);

	/*
		cudaEventCreate(&stop);
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);

	cudaEventElapsedTime(&elapsedTime, start, stop);
	printf("Elapsed time : %f ms\n", elapsedTime);
	*/
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		goto Error;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
		goto Error;
	}

	// Copy output vector from GPU buffer to host memory.
	//cudaStatus = cudaMemcpy(m_resultData, iterationsCount % 2 == 0 ? dev_data : dev_resultData, m_dataLength * sizeof(ubyte), cudaMemcpyDeviceToHost);
	cudaStatus = cudaMemcpy(m_resultData, dev_result, m_dataLength * sizeof(float), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}

Error:
	cudaFree(dev_data);
	cudaFree(dev_result);

	return cudaStatus;
}

/*
Run simple histogram and save feature vectors using measure
All images must be the same size
Input:
	std::ifstream&	fs: filenames.txt filestream
	size_t			m_imageWidth: image width
	size_t			m_imageHeight: image height
	size_t			m_dataLength: m_imageWidth*m_imageHeight
	size_t			n_bins: number of bins
	ushort			threadsCount: number of threads per block
	Measure			measure: Measure object
*/
cudaError_t cuda_setup_simple(std::ifstream& fs,
	size_t m_imageWidth, size_t m_imageHeight, size_t m_dataLength,
	size_t n_bins, ushort threadsCount,
	Measure measure)
{
	float* m_data = new float[m_dataLength];
	uint* m_bins_fv = new uint[n_bins];

	float* dev_data = 0;
	uint* dev_H_floor = 0;
	float* dev_mag = 0;
	float* dev_ori = 0;

	size_t reqBlocksCount = (m_imageWidth * m_imageHeight) / threadsCount;
	ushort blocksCount = (ushort)std::min((size_t)32768, reqBlocksCount);

	uint* dev_resultFv = 0;
	cudaError_t cudaStatus;

	cudaStatus = cudaSetDevice(0);

	cudaStatus = cudaMalloc((void**)&dev_data, m_dataLength * sizeof(float));
	cudaStatus = cudaMalloc((void**)&dev_H_floor, m_dataLength * sizeof(uint));
	//cudaStatus = cudaMalloc((void**)&dev_mag, m_dataLength * sizeof(float));
	cudaStatus = cudaMalloc((void**)&dev_ori, m_dataLength * sizeof(float));
	cudaStatus = cudaMalloc((void**)&dev_resultFv, n_bins * sizeof(uint));

	//Load image
	std::string line;
	while (std::getline(fs, line))
	{
		cv::Mat image = cv::imread(line, cv::IMREAD_GRAYSCALE);

		//Copy image bytes to 1D float array
		for (int i = 0; i < m_imageHeight; i++) 
		{
			for (int j = 0; j < m_imageWidth; j++) 
			{
				m_data[j + i * m_imageWidth] = image.at<ubyte>(i, j) / 255.0;
			}
		}

		cudaStatus = cudaMemcpy(dev_data, m_data, m_dataLength * sizeof(float), cudaMemcpyHostToDevice);
		cudaStatus = cudaMemset(dev_H_floor, 0, m_dataLength * sizeof(uint));
		cudaStatus = cudaMemset(dev_ori, 0, m_dataLength * sizeof(float));
		cudaStatus = cudaMemset(dev_resultFv, 0, n_bins * sizeof(uint));

		//Execute kernels
		gradient_o << <blocksCount, threadsCount >> > (dev_data, m_imageWidth,
			m_imageHeight, dev_ori);

		cudaStatus = cudaGetLastError();
		cudaStatus = cudaDeviceSynchronize();

		gradient_h_floor << <blocksCount, threadsCount >> > (dev_ori, m_imageWidth,
			m_imageHeight, n_bins, dev_H_floor);

		cudaStatus = cudaGetLastError();
		cudaStatus = cudaDeviceSynchronize();

		histogram_simple << <blocksCount, threadsCount >> > (dev_H_floor, m_imageWidth,
			m_imageHeight, n_bins, dev_resultFv);

		cudaStatus = cudaGetLastError();
		cudaStatus = cudaDeviceSynchronize();

		cudaStatus = cudaMemcpy(m_bins_fv, dev_resultFv, n_bins * sizeof(uint), cudaMemcpyDeviceToHost);

		measure.write_fv(m_bins_fv);
	}

	cudaFree(dev_data);
	cudaFree(dev_H_floor);
	//cudaFree(dev_mag);
	cudaFree(dev_ori);
	cudaFree(dev_resultFv);

	return cudaStatus;
}

/*
Run weighed histogram and save feature vectors using measure
All images must be the same size
Input:
	std::ifstream&	fs: filenames.txt filestream
	size_t			m_imageWidth: image width
	size_t			m_imageHeight: image height
	size_t			m_dataLength: m_imageWidth*m_imageHeight
	size_t			n_bins: number of bins
	ushort			threadsCount: number of threads per block
	Measure			measure: Measure object
*/
cudaError_t cuda_setup_pond(std::ifstream& fs,
	size_t m_imageWidth, size_t m_imageHeight, size_t m_dataLength,
	size_t n_bins, ushort threadsCount,
	Measure measure)
{
	float* m_data = new float[m_dataLength];
	float* m_bins_fv = new float[n_bins];

	float* dev_data = 0;
	uint* dev_H_floor = 0;
	float* dev_mag = 0;
	float* dev_ori = 0;

	size_t reqBlocksCount = (m_imageWidth * m_imageHeight) / threadsCount;
	ushort blocksCount = (ushort)std::min((size_t)32768, reqBlocksCount);

	float* dev_resultFv = 0;
	cudaError_t cudaStatus;

	cudaStatus = cudaSetDevice(0);

	cudaStatus = cudaMalloc((void**)&dev_data, m_dataLength * sizeof(float));
	cudaStatus = cudaMalloc((void**)&dev_H_floor, m_dataLength * sizeof(uint));
	cudaStatus = cudaMalloc((void**)&dev_mag, m_dataLength * sizeof(float));
	cudaStatus = cudaMalloc((void**)&dev_ori, m_dataLength * sizeof(float));
	cudaStatus = cudaMalloc((void**)&dev_resultFv, n_bins * sizeof(float));

	//Load image
	std::string line;
	while (std::getline(fs, line))
	{
		cv::Mat image = cv::imread(line, cv::IMREAD_GRAYSCALE);

		//Copy image bytes to 1D float array
		for (int i = 0; i < m_imageHeight; i++)
		{
			for (int j = 0; j < m_imageWidth; j++)
			{
				m_data[j + i * m_imageWidth] = image.at<ubyte>(i, j) / 255.0;
			}
		}

		cudaStatus = cudaMemcpy(dev_data, m_data, m_dataLength * sizeof(float), cudaMemcpyHostToDevice);
		cudaStatus = cudaMemset(dev_H_floor, 0, m_dataLength * sizeof(uint));
		cudaStatus = cudaMemset(dev_ori, 0, m_dataLength * sizeof(float));
		cudaStatus = cudaMemset(dev_resultFv, 0, n_bins * sizeof(float));

		//Execute kernels
		gradient_o << <blocksCount, threadsCount >> > (dev_data, m_imageWidth,
			m_imageHeight, dev_ori);

		gradient_m << <blocksCount, threadsCount >> > (dev_data, m_imageWidth,
			m_imageHeight, dev_mag);

		cudaStatus = cudaGetLastError();
		cudaStatus = cudaDeviceSynchronize();

		gradient_h_floor << <blocksCount, threadsCount >> > (dev_ori, m_imageWidth,
			m_imageHeight, n_bins, dev_H_floor);

		cudaStatus = cudaGetLastError();
		cudaStatus = cudaDeviceSynchronize();

		histogram_mag << <blocksCount, threadsCount >> > (dev_mag, dev_H_floor, m_imageWidth,
			m_imageHeight, n_bins, dev_resultFv);

		cudaStatus = cudaGetLastError();
		cudaStatus = cudaDeviceSynchronize();

		cudaStatus = cudaMemcpy(m_bins_fv, dev_resultFv, n_bins * sizeof(float), cudaMemcpyDeviceToHost);

		measure.write_fv(m_bins_fv);
	}

	cudaFree(dev_data);
	cudaFree(dev_H_floor);
	//cudaFree(dev_mag);
	cudaFree(dev_ori);
	cudaFree(dev_resultFv);

	return cudaStatus;
}

/*
Run interpolated histogram and save feature vectors using measure
All images must be the same size
Input:
	std::ifstream&	fs: filenames.txt filestream
	size_t			m_imageWidth: image width
	size_t			m_imageHeight: image height
	size_t			m_dataLength: m_imageWidth*m_imageHeight
	size_t			n_bins: number of bins
	ushort			threadsCount: number of threads per block
	Measure			measure: Measure object
*/
cudaError_t cuda_setup_pond_int_lin(std::ifstream& fs,
	size_t m_imageWidth, size_t m_imageHeight, size_t m_dataLength,
	size_t n_bins, ushort threadsCount,
	Measure measure)
{
	float* m_data = new float[m_dataLength];
	float* m_bins_fv = new float[n_bins];

	float* dev_data = 0;
	uint* dev_H_floor = 0;
	float* dev_mag = 0;
	float* dev_ori = 0;

	size_t reqBlocksCount = (m_imageWidth * m_imageHeight) / threadsCount;
	ushort blocksCount = (ushort)std::min((size_t)32768, reqBlocksCount);

	float* dev_resultFv = 0;
	cudaError_t cudaStatus;

	cudaStatus = cudaSetDevice(0);

	cudaStatus = cudaMalloc((void**)&dev_data, m_dataLength * sizeof(float));
	cudaStatus = cudaMalloc((void**)&dev_mag, m_dataLength * sizeof(float));
	cudaStatus = cudaMalloc((void**)&dev_ori, m_dataLength * sizeof(float));
	cudaStatus = cudaMalloc((void**)&dev_resultFv, n_bins * sizeof(float));

	//Load image
	std::string line;
	while (std::getline(fs, line))
	{
		cv::Mat image = cv::imread(line, cv::IMREAD_GRAYSCALE);

		//Copy image bytes to 1D float array
		for (int i = 0; i < m_imageHeight; i++)
		{
			for (int j = 0; j < m_imageWidth; j++)
			{
				m_data[j + i * m_imageWidth] = image.at<ubyte>(i, j) / 255.0;
			}
		}

		cudaStatus = cudaMemcpy(dev_data, m_data, m_dataLength * sizeof(float), cudaMemcpyHostToDevice);
		cudaStatus = cudaMemset(dev_H_floor, 0, m_dataLength * sizeof(uint));
		cudaStatus = cudaMemset(dev_ori, 0, m_dataLength * sizeof(float));
		cudaStatus = cudaMemset(dev_resultFv, 0, n_bins * sizeof(float));

		//Execute kernels
		gradient_o << <blocksCount, threadsCount >> > (dev_data, m_imageWidth,
			m_imageHeight, dev_ori);

		gradient_m << <blocksCount, threadsCount >> > (dev_data, m_imageWidth,
			m_imageHeight, dev_mag);

		cudaStatus = cudaGetLastError();
		cudaStatus = cudaDeviceSynchronize();

		histogram_mag_lin_int << <blocksCount, threadsCount >> > (dev_mag, dev_ori, m_imageWidth,
			m_imageHeight, n_bins, dev_resultFv);

		cudaStatus = cudaGetLastError();
		cudaStatus = cudaDeviceSynchronize();

		cudaStatus = cudaMemcpy(m_bins_fv, dev_resultFv, n_bins * sizeof(float), cudaMemcpyDeviceToHost);

		measure.write_fv(m_bins_fv);
	}

	cudaFree(dev_data);
	cudaFree(dev_H_floor);
	//cudaFree(dev_mag);
	cudaFree(dev_ori);
	cudaFree(dev_resultFv);

	return cudaStatus;
}
