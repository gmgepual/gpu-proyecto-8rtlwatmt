﻿#include <opencv2/opencv.hpp>

#include "kernel.h"
#include <iostream>
#include <map>
#include <string>
#include <thread>
#include <utility>
#include <fstream>
#include <iostream>
#include <sstream>

/*
All images must be the same size

Running this will calculate feature vectors using settings specified by argv for all image paths listed in filenames.txt
Details about the execution such as running time will be saved to results.txt
Feature vectors will be saved to fv.txt
All aforementioned paths are understood to begin in the current working directory

ARGS:
	mode = 0 simple, 1 weighed, 2 interpolated histograms
	im_width = image width
	im_height = image height
	n_bins = number of histogram bins
	n_images = number of images in filenames.txt
	threadcount = number of threads per block
*/
int main(int argc, char** argv)
{
	if (argc != 7)
	{
		std::cout << "Use: " << argv[0] << " -mode -img_width -img_height -n_bins -n_images -threadcount" << std::endl;
		exit(1);
	}
	int args = 1;

	int mode = std::stoi(argv[args++]);
	int im_width = std::stoi(argv[args++]);
	int im_height = std::stoi(argv[args++]);
	int n_bins = std::stoi(argv[args++]);
	int n_images = std::stoi(argv[args++]);
	int threadcount = std::stoi(argv[args++]);

	Measure measure(threadcount, im_width, im_height, n_bins, n_images);

	std::ifstream fs("filenames.txt", std::ios::in);

	measure.start_exp();

	switch (mode)
	{
	case 0:
		cuda_setup_simple(fs, im_width, im_height, im_width*im_height, n_bins, threadcount, measure);
		break;
	case 1:
		cuda_setup_pond(fs, im_width, im_height, im_width*im_height, n_bins, threadcount, measure);
		break;
	case 2:
		cuda_setup_pond_int_lin(fs, im_width, im_height, im_width*im_height, n_bins, threadcount, measure);
		break;
	}

	measure.finish_exp();
	measure.write_info(mode);

	return 0;
}




