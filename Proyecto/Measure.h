#include <chrono>
#include <iostream>
#include <string>

typedef unsigned int uint;

class Measure {
public:
	Measure(int threads, int m_W, int m_H, int n_bins, int n_images);
	void start_exp();
	void finish_exp();
	double get_run_time();
	int get_Width();
	int get_Height();
	int get_threads_count();
	int get_n_bins();
	int get_n_images();
	void write_info(int mode);
	void write_fv(float*& fv);
	void write_fv(uint*& fv);

private:
	int threadsCount;
	int m_Width;
	int m_Height;
	int n_bins;
	int n_images;
	std::chrono::high_resolution_clock::time_point start, finish;


};