import matplotlib.pyplot as plt
from skimage import io
from skimage.filters import *
import timeit
import pai.basic as pai
import numpy as np
import math
import os
import os.path
import argparse

def rgbToGray(image):
	red = image[:,:,0]
	green = image[:,:,1]
	blue = image[:,:,2]
	imagegray = red * 0.299 + green * 0.587 + blue * 0.114
	return pai.toUINT8(imagegray)

def bordesVerticales(image):
	return sobel_v(image)

def bordesHorizontales(image):
	return sobel_h(image)

def magnitudGradiente(gradientv, gradienth):
	return abs(gradientv) + abs(gradienth)

def angulosGradiente(gradientv, gradienth):
	angulos = np.arctan2(gradienth, gradientv)
	booleanmatrix = angulos < 0
	angulos[booleanmatrix] += math.pi
	if(angulos.any() < 0): return -1
	return angulos

def magnitudYGradiente(image):
	gradientv = bordesVerticales(image)
	gradienth = bordesHorizontales(image)
	magnitud = magnitudGradiente(gradientv, gradienth)
	angulos = angulosGradiente(gradientv, gradienth)
	return (magnitud, angulos)

def featureVectorConteoSimple2(image, K):

	angulos = magnitudYGradiente(image)[1]

	angulosApplyHash = np.uint8(np.round(angulos / math.pi * K) % K)
	histogram = np.zeros(K, np.uint64)

	for k in range(K):
		histogram[k] = np.sum(angulosApplyHash == k)

	norma = np.linalg.norm(histogram)
	return histogram / norma


def featureVectorConteoPonderado2(image, K):
	magnitud, angulos = magnitudYGradiente(image)

	angulosApplyHash = np.uint8(np.round(angulos / math.pi * K) % K)
	histogram = np.zeros(K)

	for k in range(K):
		histogram[k] = np.sum(magnitud[angulosApplyHash == k])

	norma = np.linalg.norm(histogram)
	return histogram / norma



def featureVectorPonderadoInterpolado2(image, K):
	magnitud, angulos = magnitudYGradiente(image)
	angulosApplyHash = (angulos / math.pi * K) % K
	angulosApplyHashFloor = np.floor(angulosApplyHash)
	angulosApplyHashCeiling = np.ceil(angulosApplyHash)

	histogram = np.zeros(K+1,dtype=float)

	magnitudLow = np.multiply(magnitud, angulosApplyHashCeiling - angulosApplyHash)
	magnitudHigh = np.multiply(magnitud, np.ones(angulosApplyHash.shape) - (angulosApplyHashCeiling - angulosApplyHash))

	for k in range(K):
		histogram[k] = np.sum(magnitudLow[angulosApplyHashFloor == k]) + np.sum(magnitudHigh[angulosApplyHashCeiling == k])

	norma = np.linalg.norm(histogram)
	return histogram / norma


def generateData6(fvGenerator, K):
	imageindex = 0
	if (fvGenerator.__name__ == "featureVectorPonderadoInterpolado2"):
		datafile = np.ndarray(shape=(11 * 600, K + 1), dtype=float)#h=1
	else:
		datafile = np.ndarray(shape=(11 * 600, K + 0), dtype=float)#h=0
	for chr in os.scandir('./files/'):
		for filename in os.scandir(chr.path):
			image = io.imread(filename.path)
			fv = fvGenerator(image, K)
			datafile[imageindex] = fv
			imageindex += 1
	filepath = "./data/" + fvGenerator.__name__ + "/K=" + str(K) + "/"
	if not (os.path.exists(filepath)):
		os.makedirs(filepath)
	np.save(filepath + "/datafile.npy", datafile)
	return 0

#usar npy en vez de lista rasca
def averagePrecision2(chrTipo, listaCaracteresOrdenados):
	relevantes = listaCaracteresOrdenados[:] == chrTipo
	suma = np.sum(relevantes)
	if suma == 0: return 0
	acumulacion = np.cumsum(relevantes)
	range = np.arange(1, listaCaracteresOrdenados.shape[0] + 1)
	AP = np.sum( relevantes*acumulacion/range )/suma
	return AP


def generalMeanAPfunction4(K, featureVectorFunction, maxrel=10):

	start1 = timeit.default_timer()
	pathToLoad = "./data/" + featureVectorFunction.__name__ + "/K=" + str(K) + "/datafile.npy"
	loadedData = np.load(pathToLoad)
	chrIndex = np.arange(start=0, stop=11,step=1/600).astype(int)
	mAP = 0
	chrActual = 0
	imgNumber = 0

	for chr in os.scandir('./test/'):
		for filename in os.scandir(chr.path):
			testImgActual = io.imread(filename.path)
			FVActual = featureVectorFunction(testImgActual, K)
			testImgCopies = np.tile(FVActual, (11*600, 1))
			testImgCopies[:] = FVActual
			chrData = np.copy(chrIndex)
			FVData = loadedData[:]
			start2 = timeit.default_timer()
			distanceArray = np.linalg.norm(FVData - testImgCopies, axis=1)
			stop2 = timeit.default_timer()
			print(stop2-start2);
			sortedData = chrData[distanceArray[:].argsort()]
			averagePrecision = averagePrecision2(chrActual, sortedData[:maxrel])
			mAP += averagePrecision
			imgNumber += 1

		chrActual += 1

	stop1 = timeit.default_timer()
	print('Tiempo tardado:', stop1 - start1)
	print('mAP =',mAP/imgNumber)
	return mAP/imgNumber


def genDataMain(K, genFunc):
	start1 = timeit.default_timer()
	genFunc(featureVectorConteoSimple2, K)
	stop1 = timeit.default_timer()
	
	start2 = timeit.default_timer()
	genFunc(featureVectorConteoPonderado2, K)
	stop2 = timeit.default_timer()
	
	start3 = timeit.default_timer()
	genFunc(featureVectorPonderadoInterpolado2, K)
	stop3 = timeit.default_timer()

	print('Tiempo en generar data K=',K,':', stop1 - start1)
	print('Tiempo en generar data K=',K,':', stop2 - start2)
	print('Tiempo en generar data K=',K,':', stop3 - start3)
	
	with open("times.txt", "a+") as f:
		f.write(str(K))
		f.write(",")
		f.write(str(stop1 - start1))
		f.write(",")
		f.write(str(stop2 - start2))
		f.write(",")
		f.write(str(stop3 - start3))
		f.write("\n")


def getEverything():


	generalMeanAPfunction4(4, featureVectorConteoSimple2)
	generalMeanAPfunction4(8, featureVectorConteoSimple2)
	generalMeanAPfunction4(16, featureVectorConteoSimple2)
	generalMeanAPfunction4(32, featureVectorConteoSimple2)
	generalMeanAPfunction4(64, featureVectorConteoSimple2)
	generalMeanAPfunction4(96, featureVectorConteoSimple2)
	generalMeanAPfunction4(128, featureVectorConteoSimple2)

	generalMeanAPfunction4(4, featureVectorConteoPonderado2)
	generalMeanAPfunction4(8, featureVectorConteoPonderado2)
	generalMeanAPfunction4(16, featureVectorConteoPonderado2)
	generalMeanAPfunction4(32, featureVectorConteoPonderado2)
	generalMeanAPfunction4(64, featureVectorConteoPonderado2)
	generalMeanAPfunction4(96, featureVectorConteoPonderado2)
	generalMeanAPfunction4(128, featureVectorConteoPonderado2)

	generalMeanAPfunction4(4, featureVectorPonderadoInterpolado2)
	generalMeanAPfunction4(8, featureVectorPonderadoInterpolado2)
	generalMeanAPfunction4(16, featureVectorPonderadoInterpolado2)
	generalMeanAPfunction4(32, featureVectorPonderadoInterpolado2)
	generalMeanAPfunction4(64, featureVectorPonderadoInterpolado2)
	generalMeanAPfunction4(96, featureVectorPonderadoInterpolado2)
	generalMeanAPfunction4(128, featureVectorPonderadoInterpolado2)



def main():
	parser = argparse.ArgumentParser()

	parser.add_argument("K", help="select K to use (16, 32, 64, 96, 128)", type=int)
	parser.add_argument("function", help="select function to use (simple, weighed, interpolation)")
	parser.add_argument("--maxRelevance", help="select length of ranking for AP (max is 6600)", default=6600, type=int,
						dest='maxRel')
	args = parser.parse_args()
	K = args.K
	func = args.function
	if func=="simple":
		func=featureVectorConteoSimple2
	elif func=="weighed":
		func=featureVectorConteoPonderado2
	elif func=="interpolation":
	   func=featureVectorPonderadoInterpolado2
	else:
		exit("invalid function")
	maxRelevance = args.maxRel
	generalMeanAPfunction4(K, func, maxRelevance)

genDataMain(16, generateData6)
genDataMain(32, generateData6)
genDataMain(64, generateData6)
genDataMain(96, generateData6)
genDataMain(128, generateData6)